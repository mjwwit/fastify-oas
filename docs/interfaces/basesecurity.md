[fastify-oas](../README.md) > [BaseSecurity](../interfaces/basesecurity.md)

# Interface: BaseSecurity

## Hierarchy

**BaseSecurity**

↳  [BasicAuthenticationSecurity](basicauthenticationsecurity.md)

↳  [ApiKeySecurity](apikeysecurity.md)

↳  [BaseOAuthSecuirty](baseoauthsecuirty.md)

## Index

### Properties

* [description](basesecurity.md#description)
* [type](basesecurity.md#type)

---

## Properties

<a id="description"></a>

### `<Optional>` description

**● description**: *`string`*

*Defined in node_modules/@types/swagger-schema-official/index.d.ts:175*

___
<a id="type"></a>

###  type

**● type**: *`string`*

*Defined in node_modules/@types/swagger-schema-official/index.d.ts:174*

___

