[fastify-oas](../README.md) > [OAuth2ApplicationSecurity](../interfaces/oauth2applicationsecurity.md)

# Interface: OAuth2ApplicationSecurity

## Hierarchy

↳  [BaseOAuthSecuirty](baseoauthsecuirty.md)

**↳ OAuth2ApplicationSecurity**

## Index

### Properties

* [description](oauth2applicationsecurity.md#description)
* [flow](oauth2applicationsecurity.md#flow)
* [scopes](oauth2applicationsecurity.md#scopes)
* [tokenUrl](oauth2applicationsecurity.md#tokenurl)
* [type](oauth2applicationsecurity.md#type)

---

## Properties

<a id="description"></a>

### `<Optional>` description

**● description**: *`string`*

*Inherited from [BaseSecurity](basesecurity.md).[description](basesecurity.md#description)*

*Defined in node_modules/@types/swagger-schema-official/index.d.ts:175*

___
<a id="flow"></a>

###  flow

**● flow**: *`string`*

*Inherited from [BaseOAuthSecuirty](baseoauthsecuirty.md).[flow](baseoauthsecuirty.md#flow)*

*Defined in node_modules/@types/swagger-schema-official/index.d.ts:189*

___
<a id="scopes"></a>

### `<Optional>` scopes

**● scopes**: *[OAuthScope](oauthscope.md)[]*

*Defined in node_modules/@types/swagger-schema-official/index.d.ts:203*

___
<a id="tokenurl"></a>

###  tokenUrl

**● tokenUrl**: *`string`*

*Defined in node_modules/@types/swagger-schema-official/index.d.ts:202*

___
<a id="type"></a>

###  type

**● type**: *`string`*

*Inherited from [BaseSecurity](basesecurity.md).[type](basesecurity.md#type)*

*Defined in node_modules/@types/swagger-schema-official/index.d.ts:174*

___

