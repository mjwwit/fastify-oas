[fastify-oas](../README.md) > [ExamplesObject](../interfaces/examplesobject.md)

# Interface: ExamplesObject

## Hierarchy

**ExamplesObject**

## Indexable

\[name: `string`\]:&nbsp;[ExampleObject](exampleobject.md) \| [ReferenceObject](referenceobject.md)
## Index

---

